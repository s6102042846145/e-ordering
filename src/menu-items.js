export default {
    items: [
        {
            id: 'support',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-support',
            children: [
                {
                    id: 'sample-page',
                    title: 'Sample Page',
                    type: 'item',
                    url: '/sample-page',
                    classes: 'nav-item',
                    icon: 'feather icon-sidebar'
                },
                {
                    id: 'pn-master',
                    title: 'PN MASTER',
                    type: 'collapse',
                    icon: 'feather icon-book',
                    children: [
                        
                        {
                            id: 'product',
                            title: 'PRODUCT',
                            type: 'item',
                            url: '/pn-master/product'
                        },
                        {
                            id: 'product-master',
                            title: 'PRODUCT MASTER',
                            type: 'item',
                            url: '/pn-master/product-master'
                        },
                        {
                            id: 'orter-master',
                            title: 'OTHER MASTER',
                            type: 'item',
                            url: '/pn-master/other-master'
                        },
                        {
                            id: 'product-plan',
                            title: 'PRODUCTION PLAN',
                            type: 'item',
                            url: '/pn-master/production-plan'
                        },
                    ]
                },
                {
                    id: 'authorizes',
                    title: 'AUTHORIZES',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    children: [
                        {
                            id: 'user-authorize',
                            title: 'USER & AUTHORIZE',
                            type: 'item',
                            url: '/authorize/user-authorize'
                        },
                        {
                            id: 'e-mail',
                            title: 'E-MAIL',
                            type: 'item',
                            url: '/authorize/e-mail'
                        }
                        ,
                        {
                            id: 'set-screen',
                            title: 'SET SCREEN',
                            type: 'item',
                            url: '/authorize/set-screen'
                        }
                    ]
                }
            ]
        }
    ]
}