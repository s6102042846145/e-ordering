import React from 'react';


//const OtherSamplePage = React.lazy(() => import('./Demo/Other/SamplePage'));
const OtherSamplePage = React.lazy(() => import('./admin/login/index'));

//PN-Master => Product
const ProductMain = React.lazy(() => import('./pn-master/product/index'));
const ProductCodeOpenningMS126 = React.lazy(() => import('./pn-master/product/ProductCodeOpenningMS126'));
const ConfirmOpenProductSBC = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSBC'));
const ConfirmOpenProductSSK = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSK'));
const ConfirmOpenProductSSR2 = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSR2'));
const ConfirmOpenProductSGT = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSGT'));
const ConfirmOpenProductSSJ = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSJ'));
const ConfirmOpenProductSSR1 = React.lazy(() => import('./pn-master/product/ConfirmOpenProductSSR1'));
const ItemMastFGSTOCK = React.lazy(() => import('./pn-master/product/ItemMastFGSTOCK'));

//PN-Master => Product Master
const ProductMasterMain = React.lazy(() => import('./pn-master/product-master/index'));
const GradeMaster = React.lazy(() => import('./pn-master/product-master/GradeMaster'));
const GroupVcCost = React.lazy(() => import('./pn-master/product-master/GroupVcCost'));
const GroupVcMaster = React.lazy(() => import('./pn-master/product-master/GroupVcMaster'));
const PnStatus = React.lazy(() => import('./pn-master/product-master/PnStatus'));
const PnStatusSize = React.lazy(() => import('./pn-master/product-master/PnStatusSize'));
const SalesOrgChannelNewPn = React.lazy(() => import('./pn-master/product-master/SalesOrgChannelNewPn'));
const SectionHierarchyMaster = React.lazy(() => import('./pn-master/product-master/SectionHierarchyMaster'));
const SectionMaster = React.lazy(() => import('./pn-master/product-master/SectionMaster'));
const SizeMaster = React.lazy(() => import('./pn-master/product-master/SizeMaster'));
const StandardGrade = React.lazy(() => import('./pn-master/product-master/StandardGrade'));
const StandardMaster = React.lazy(() => import('./pn-master/product-master/StandardMaster'));
const StorageNewPn = React.lazy(() => import('./pn-master/product-master/StorageNewPn'));

//PN-Master => Other Master
const OtherMasterMain = React.lazy(() => import('./pn-master/other-master/index'));
const PclLicence = React.lazy(() => import('./pn-master/other-master/PclLicence'));
const NominalSizeHarmonize = React.lazy(() => import('./pn-master/other-master/NominalSizeHarmonize'));
const SectionHarmonize = React.lazy(() => import('./pn-master/other-master/SectionHarmonize'));
const PclLicenceDesc = React.lazy(() => import('./pn-master/other-master/PclLicenceDesc'));

//PN-Master => Production Plan
const ProductionPlanMain = React.lazy(() => import('./pn-master/production-plan/index'));
const ExportYearlyProductionPlan = React.lazy(() => import('./pn-master/production-plan/ExportYearlyProductionPlan'));
const DomesticYearlyProductionPlan = React.lazy(() => import('./pn-master/production-plan/DomesticYearlyProductionPlan'));





const routes = [
   // { path: '/sample-page', exact: true, name: 'Sample Page', component: OtherSamplePage },
   { path: '/sample-page', exact: true, name: 'Login', component: OtherSamplePage },

   //PN-Master => Product
   { path: '/pn-master/product/', exact: true, name: 'Product', component: ProductMain },
   { path: '/pn-master/product/ProductCodeOpenningMS126', exact: true, name: 'ใบเปิดรหัสสินค้า (MS126-1)', component: ProductCodeOpenningMS126 },
   { path: '/pn-master/product/ConfirmOpenProductSBC', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สบช)', component: ConfirmOpenProductSBC },
   { path: '/pn-master/product/ConfirmOpenProductSSK', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสค)', component: ConfirmOpenProductSSK },
   { path: '/pn-master/product/ConfirmOpenProductSSR2', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสร2)', component: ConfirmOpenProductSSR2 },
   { path: '/pn-master/product/ConfirmOpenProductSGT', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สกธ)', component: ConfirmOpenProductSGT },
   { path: '/pn-master/product/ConfirmOpenProductSSJ', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสจ)', component: ConfirmOpenProductSSJ },
   { path: '/pn-master/product/ConfirmOpenProductSSR1', exact: true, name: 'ยืนยันเปิดรหัสสินค้า (สสร1)', component: ConfirmOpenProductSSR1 },
   { path: '/pn-master/product/ItemMastFGSTOCK', exact: true, name: 'ITEM MAST (FGSTOCK)', component: ItemMastFGSTOCK },

   //PN-Master => Product Master
   { path: '/pn-master/product-master/', exact: true, name: 'Product Master', component: ProductMasterMain },
   { path: '/pn-master/product-master/SectionMaster', exact: true, name: 'Section Master', component: SectionMaster },
   { path: '/pn-master/product-master/GradeMaster', exact: true, name: 'Grade Master', component: GradeMaster},
   { path: '/pn-master/product-master/GroupVcCost', exact: true, name: 'Group VC Cost', component: GroupVcCost },
   { path: '/pn-master/product-master/GroupVcMaster', exact: true, name: 'Group VC Master', component: GroupVcMaster },
   { path: '/pn-master/product-master/PnStatus', exact: true, name: 'PN Status', component: PnStatus },
   { path: '/pn-master/product-master/PnStatusSize', exact: true, name: 'PN Status Size', component: PnStatusSize },
   { path: '/pn-master/product-master/SalesOrgChannelNewPn', exact: true, name: 'Sales ORG Channel New Pn', component: SalesOrgChannelNewPn },
   { path: '/pn-master/product-master/SectionHierarchyMaster', exact: true, name: 'Section Hierarchy Master', component: SectionHierarchyMaster },
   { path: '/pn-master/product-master/SizeMaster', exact: true, name: 'Size Master', component: SizeMaster },
   { path: '/pn-master/product-master/StandardGrade', exact: true, name: 'Standard Grade', component: StandardGrade },
   { path: '/pn-master/product-master/StandardMaster', exact: true, name: 'Standard Master', component: StandardMaster },
   { path: '/pn-master/product-master/StorageNewPn', exact: true, name: 'Storage New PN', component: StorageNewPn },
   
   //PN-Master => Other Master
   { path: '/pn-master/other-master', exact: true, name: 'Other Master', component: OtherMasterMain },
   { path: '/pn-master/other-master/PclLicence', exact: true, name: 'PCL Licence', component: PclLicence },
   { path: '/pn-master/other-master/NominalSizeHarmonize', exact: true, name: 'Nominal Size Harmonize', component: NominalSizeHarmonize },
   { path: '/pn-master/other-master/SectionHarmonize', exact: true, name: 'Section Harmonize', component: SectionHarmonize },
   { path: '/pn-master/other-master/PclLicenceDesc', exact: true, name: 'PCL Licence Description', component: PclLicenceDesc },

   //PN-Master => Production Plan
   { path: '/pn-master/production-plan', exact: true, name: 'Production Plan', component: ProductionPlanMain },
   { path: '/pn-master/production-plan/ExportYearlyProductionPlan', exact: true, name: 'Export Yearly Production Plan', component: ExportYearlyProductionPlan },
   { path: '/pn-master/production-plan/DomesticYearlyProductionPlan', exact: true, name: 'Domestic Yearly Production Plan', component: DomesticYearlyProductionPlan },

];

export default routes;