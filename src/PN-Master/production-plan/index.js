import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {
    render() {
        return (
            <Aux>
                <Row>
                    <Col xl={6}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Production Plan </Card.Title>
                            </Card.Header>
                                <Nav variant="pills" className="flex-column m-0">
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/production-plan/ExportYearlyProductionPlan"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Export Yearly Production Plan</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/production-plan/DomesticYearlyProductionPlan"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Domestic Yearly Production Plan</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;