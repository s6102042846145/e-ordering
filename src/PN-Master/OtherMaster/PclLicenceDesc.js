import React from 'react';
import { Row, Col, Card, Form, Button, Tabs, Tab, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');


const dataGrade = [
    {
        "id": 1,
        "Col1": "12",
        "Col2": "JIS - JIS G3101-2004",
        "Col3": "SS540",
        "Col4": "NULL",
        "Col5": "09-Jan-20 10:42:19",
    }
];

const dataSize = [
    {
        "id": 1,
        "Col1": "1",
        "Col2": "L",
        "Col3": "",
        "Col4": "19.6",
        "Col5": "130x130x10x10",
        "Col6": "N",
        "Col7": "L 130 x 130 x 10 mm",
        "Col8": "L 130 x 130 x 10 mm",
        "Col9": "mm X mm X mm",
        "Col10": "Equal Angle",
        "Col11": "08-Jan-20 10:26:00",
        "Col12": "0"
    },
    {
        "id": 2,
        "Col1": "1",
        "Col2": "L",
        "Col3": "100X100X10",
        "Col4": "14.9",
        "Col5": "100x100x10x10",
        "Col6": "N",
        "Col7": "L 100 x 100 x 10 mm",
        "Col8": "L 100 x 100 x 10 mm",
        "Col9": "mm X mm X mm",
        "Col10": "Equal Angle",
        "Col11": "08-Jan-20 10:26:00",
        "Col12": "0"
    },
    {
        "id": 3,
        "Col1": "1",
        "Col2": "L",
        "Col3": "100X12",
        "Col4": "17.8",
        "Col5": "100x100x12x12",
        "Col6": "N",
        "Col7": "L 100 x 100 x 12 mm",
        "Col8": "L 100 x 100 x 12 mm",
        "Col9": "mm X mm X mm",
        "Col10": "Equal Angle",
        "Col11": "08-Jan-20 10:26:00",
        "Col12": "0"
    },
    {
        "id": 4,
        "Col1": "1",
        "Col2": "L",
        "Col3": "100X13",
        "Col4": "19.1",
        "Col5": "100x100x13x13",
        "Col6": "N",
        "Col7": "L 100 x 100 x 13 mm",
        "Col8": "L 100 x 100 x 13 mm",
        "Col9": "mm X mm X mm",
        "Col10": "Equal Angle",
        "Col11": "08-Jan-20 10:26:00",
        "Col12": "0"
    }
];

const dataExcept = [];

const dataSectNameFormat = [
    {
        "id": 1,
        "Col1": "L ANGLE",
        "Col2": "1",
        "Col3": "-product name format-",
        "Col4": "05-May-20 13:50:22"
    }
];

function atable() {
    let tableGrade = '#tableGrade';
    let tableSize = '#tableSize';
    let tableExcept = '#tableExcept';
    let tableSectNameFormat = '#tableSectNameFormat';

    $.fn.dataTable.ext.errMode = 'throw';

    $(tableGrade).DataTable({
        data: dataGrade,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } }
        ]
    });

    $(tableSize).DataTable({
        data: dataSize,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } },
            { "data": "Col6", render: function (data, type, row) { return data; } },
            { "data": "Col7", render: function (data, type, row) { return data; } },
            { "data": "Col8", render: function (data, type, row) { return data; } },
            { "data": "Col9", render: function (data, type, row) { return data; } },
            { "data": "Col10", render: function (data, type, row) { return data; } },
            { "data": "Col11", render: function (data, type, row) { return data; } },
            { "data": "Col12", render: function (data, type, row) { return data; } }
        ]
    });

    $(tableExcept).DataTable({
        data: dataExcept,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } },
            { "data": "Col6", render: function (data, type, row) { return data; } },
            { "data": "Col7", render: function (data, type, row) { return data; } },
            { "data": "Col8", render: function (data, type, row) { return data; } },
            { "data": "Col9", render: function (data, type, row) { return data; } }
        ]
    });

    $(tableSectNameFormat).DataTable({
        data: dataSectNameFormat,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } }
        ]
    });
}

class PclLicenceDesc extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#grade-select-all').click(function (event) {
            var checkboxes = $('#tableGrade tbody input[type="checkbox"]');
            var totalCheckboxes = checkboxes.length;
            if (this.checked) {
                $('#tableGrade :checkbox').each(function () {
                    if (totalCheckboxes === 1) {
                        $('#btnDelGrade').removeClass('d-none');
                        $('#btnEditGrade').removeClass('d-none');
                        this.checked = true;
                    } else {
                        $('#btnDelGrade').removeClass('d-none');
                        $('#btnEditGrade').addClass('d-none');
                        this.checked = true;
                    }
                });
            } else {
                $('#tableGrade :checkbox').each(function () {
                    $('#btnDelGrade').addClass('d-none');
                    $('#btnEditGrade').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#tableGrade tbody').on('click', 'input[type="checkbox"]', function (event) {
            var checkboxes = $('#tableGrade tbody input[type="checkbox"]');
            var numberOfChecked = checkboxes.filter(':checked').length
            var totalCheckboxes = checkboxes.length;
            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if ((numberOfChecked === totalCheckboxes) && (totalCheckboxes !== 1)) {
                $('#btnDelGrade').removeClass('d-none');
                $('#btnEditGrade').addClass('d-none');
                $('#grade-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDelGrade').removeClass('d-none');
                $('#btnEditGrade').removeClass('d-none');
                $('#grade-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDelGrade').removeClass('d-none');
                $('#btnEditGrade').addClass('d-none');
                $('#grade-select-all').prop('checked', false);
            } else {
                $('#btnDelGrade').addClass('d-none');
                $('#btnEditGrade').addClass('d-none');
            }
        });

        $('#size-select-all').click(function (event) {
            var checkboxes = $('#tableSize tbody input[type="checkbox"]');
            var totalCheckboxes = checkboxes.length;
            if (this.checked) {
                $('#tableSize :checkbox').each(function () {
                    if (totalCheckboxes === 1) {
                        $('#btnDelSize').removeClass('d-none');
                        $('#btnEditSize').removeClass('d-none');
                        this.checked = true;
                    } else {
                        $('#btnDelSize').removeClass('d-none');
                        $('#btnEditSize').addClass('d-none');
                        this.checked = true;
                    }
                });
            } else {
                $('#tableSize :checkbox').each(function () {
                    $('#btnDelSize').addClass('d-none');
                    $('#btnEditSize').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#tableSize tbody').on('click', 'input[type="checkbox"]', function (event) {
            var checkboxes = $('#tableSize tbody input[type="checkbox"]');
            var numberOfChecked = checkboxes.filter(':checked').length
            var totalCheckboxes = checkboxes.length;
            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if ((numberOfChecked === totalCheckboxes) && (totalCheckboxes !== 1)) {
                $('#btnDelSize').removeClass('d-none');
                $('#btnEditSize').addClass('d-none');
                $('#size-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDelSize').removeClass('d-none');
                $('#btnEditSize').removeClass('d-none');
                $('#size-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDelSize').removeClass('d-none');
                $('#btnEditSize').addClass('d-none');
                $('#size-select-all').prop('checked', false);
            } else {
                $('#btnDelSize').addClass('d-none');
                $('#btnEditSize').addClass('d-none');
            }
        });

        $('#except-select-all').click(function (event) {
            var checkboxes = $('#tableExcept tbody input[type="checkbox"]');
            var totalCheckboxes = checkboxes.length;
            if (this.checked) {
                $('#tableExcept :checkbox').each(function () {
                    if (totalCheckboxes === 1) {
                        $('#btnDelSize').removeClass('d-none');
                        $('#btnEditSize').removeClass('d-none');
                        this.checked = true;
                    } else {
                        $('#btnDelSize').removeClass('d-none');
                        $('#btnEditSize').addClass('d-none');
                        this.checked = true;
                    }
                });
            } else {
                $('#tableExcept :checkbox').each(function () {
                    $('#btnDelSize').addClass('d-none');
                    $('#btnEditSize').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#tableExcept tbody').on('click', 'input[type="checkbox"]', function (event) {
            var checkboxes = $('#tableExcept tbody input[type="checkbox"]');
            var numberOfChecked = checkboxes.filter(':checked').length
            var totalCheckboxes = checkboxes.length;
            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if ((numberOfChecked === totalCheckboxes) && (totalCheckboxes !== 1)) {
                $('#btnDelExcept').removeClass('d-none');
                $('#btnEditExcept').addClass('d-none');
                $('#except-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDelExcept').removeClass('d-none');
                $('#btnEditExcept').removeClass('d-none');
                $('#except-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDelExcept').removeClass('d-none');
                $('#btnEditExcept').addClass('d-none');
                $('#except-select-all').prop('checked', false);
            } else {
                $('#btnDelExcept').addClass('d-none');
                $('#btnEditExcept').addClass('d-none');
            }
        });

        $('#sectNameFormat-select-all').click(function (event) {
            var checkboxes = $('#tableSectNameFormat tbody input[type="checkbox"]');
            var totalCheckboxes = checkboxes.length;
            if (this.checked) {
                $('#tableSectNameFormat :checkbox').each(function () {
                    if (totalCheckboxes === 1) {
                        $('#btnDelSectNameFormat').removeClass('d-none');
                        $('#btnEditSectNameFormat').removeClass('d-none');
                        this.checked = true;
                    } else {
                        $('#btnDelSectNameFormat').removeClass('d-none');
                        $('#btnEditSectNameFormat').addClass('d-none');
                        this.checked = true;
                    }
                });
            } else {
                $('#tableSectNameFormat :checkbox').each(function () {
                    $('#btnDelSectNameFormat').addClass('d-none');
                    $('#btnEditSectNameFormat').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#tableSectNameFormat tbody').on('click', 'input[type="checkbox"]', function (event) {
            var checkboxes = $('#tableSectNameFormat tbody input[type="checkbox"]');
            var numberOfChecked = checkboxes.filter(':checked').length
            var totalCheckboxes = checkboxes.length;
            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if ((numberOfChecked === totalCheckboxes) && (totalCheckboxes !== 1)) {
                $('#btnDelSectNameFormat').removeClass('d-none');
                $('#btnEditSectNameFormat').addClass('d-none');
                $('#sectNameFormat-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDelSectNameFormat').removeClass('d-none');
                $('#btnEditSectNameFormat').removeClass('d-none');
                $('#sectNameFormat-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDelSectNameFormat').removeClass('d-none');
                $('#btnEditSectNameFormat').addClass('d-none');
                $('#sectNameFormat-select-all').prop('checked', false);
            } else {
                $('#btnDelSectNameFormat').addClass('d-none');
                $('#btnEditSectNameFormat').addClass('d-none');
            }
        });

        return (
            
            <Aux>
                <Row>
                    <Col>
                        <a className='btn btn-secondary feather icon-arrow-left' href='/PN-Master/OtherMaster/PclLicence'> ย้อนกลับ </a>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>

                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">DESCRIPTION</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>PLANT</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select" value={"SYS1"}>
                                                                <option value="NONE">None</option><option value="SYS1">SYS1</option><option value="SYS2">SYS2</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>COUNTRY NAME</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="MY Malaysia" />   
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>PLC.NO.</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="PC002218" />    
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>VALIDITY FROM</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} defaultValue="04/30/19" inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>TO DATE</Form.Label>
                                                        <Col sm={8}>
                                                            <Datetime renderInput={this.renderInput} timeFormat={false} defaultValue="04/30/20" inputProps={{ placeholder: 'Select Date' }} />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>PRODUCT</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="-product-" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>

                                <Card>
                                    <Card.Body>
                                        <Tabs defaultActiveKey="grade" className="mb-3">
                                            <Tab eventKey="grade" title="GRADE">
                                                <Row>
                                                    <Col className="btn-page text-left" sm>
                                                        <Button id="btnEditGrade" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                                        <Button id="btnDelGrade" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                                    </Col>
                                                    <Col className="btn-page text-right" sm>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                                    </Col>
                                                </Row>
                                                <br />
                                                <Table ref="tbl" striped hover responsive bordered id="tableGrade">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="grade-select-all" /></th>
                                                            <th>#</th>
                                                            <th>STD-GRADE CD.</th>
                                                            <th>GRADE STD.</th>
                                                            <th>GRADE</th>
                                                            <th>REMARK.-</th>
                                                            <th>UPDATED DATE</th>
                                                        </tr>
                                                    </thead>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="size" title="SIZE">
                                                <Row>
                                                    <Col className="btn-page text-left" sm>
                                                        <Button id="btnEditSize" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                                        <Button id="btnDelSize" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                                    </Col>
                                                    <Col className="btn-page text-right" sm>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                                    </Col>
                                                </Row>
                                                <br />
                                                <Table ref="tbl" striped hover responsive bordered id="tableSize">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="size-select-all" /></th>
                                                            <th>#</th>
                                                            <th>GRP#</th>
                                                            <th>SEC</th>
                                                            <th>NOMINALSIZE.</th>
                                                            <th>WEIGHT(KG./M)</th>
                                                            <th>HxBxT1xT2(MM.)</th>
                                                            <th>INCH.FLAG</th>
                                                            <th>SIZE NAME(MM)</th>
                                                            <th>SIZE NAME(INCH)</th>
                                                            <th>REMARK.-</th>
                                                            <th>REMARK1.-</th>
                                                            <th>UPDATED DATE</th>
                                                            <th>EXCEPT</th>
                                                        </tr>
                                                    </thead>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="except" title="EXCEPT">
                                                <Row>
                                                    <Col className="btn-page text-left" sm>
                                                        <Button id="btnEditExcept" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                                        <Button id="btnDelExcept" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                                    </Col>
                                                    <Col className="btn-page text-right" sm>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                                    </Col>
                                                </Row>
                                                <br />
                                                <Table ref="tbl" striped hover responsive bordered id="tableExcept">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="except-select-all" /></th>
                                                            <th>#</th>
                                                            <th>DIM.STD.</th>
                                                            <th>SEC</th>
                                                            <th>NOMINALSIZE.</th>
                                                            <th>WEIGHT(KG.)</th>
                                                            <th>HxBxT1xT2(MM.)</th>
                                                            <th>INCH.FLAG</th>
                                                            <th>STD.-GRADE.</th>
                                                            <th>REMARK.-</th>
                                                            <th>UPDATED DATE</th>
                                                        </tr>
                                                    </thead>
                                                </Table>
                                            </Tab>
                                            <Tab eventKey="sectNameFormat" title="SECT-NAME FORMAT">
                                                <Row>
                                                    <Col className="btn-page text-left" sm>
                                                        <Button id="btnEditSectNameFormat" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                                        <Button id="btnDelSectNameFormat" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                                    </Col>
                                                    <Col className="btn-page text-right" sm>
                                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                                    </Col>
                                                </Row>
                                                <br />
                                                <Table ref="tbl" striped hover responsive bordered id="tableSectNameFormat">
                                                    <thead>
                                                        <tr>
                                                            <th><Form.Check id="sectNameFormat-select-all" /></th>
                                                            <th>#</th>
                                                            <th>SECTION</th>
                                                            <th>GRP#</th>
                                                            <th>NAME FORMAT.-</th>
                                                            <th>UPDATED DATE</th>
                                                        </tr>
                                                    </thead>
                                                </Table>
                                            </Tab>
                                        </Tabs>
                                    </Card.Body>
                                </Card>

                                <Row>
                                    <Col md={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={8}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>DESCRIPTION OF GOODS 1</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="3"  />   
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={8}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>DESCRIPTION OF GOODS 2</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />   
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={8}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}></Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Check label="CANCELED" /> 
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>วันที่สร้าง</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="30-Apr-19 00:00" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>สร้างโดย</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="anothaid@sys" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SYSTEM.CODE</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="8" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>วันที่แก้ไข</Form.Label>
                                                        <Col sm={8}>
                                                        <Form.Control type="text" value=" 08-Jan-20 10:31" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>แก้ไขโดย</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" value="anothaid@sys" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default PclLicenceDesc;
