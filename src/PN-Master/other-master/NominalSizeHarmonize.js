import React from 'react';
import { Row, Col, Card, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );


const names = [
    {
        "id": 1,
        "Col1": "Letter",
        "Col2": "",
        "Col3": "I-BEAM",
        "Col4": "300X150M",
        "Col5": "65.5",
        "Col6": "Beam",
        "Col7": "H-BEAM",
        "Col8": "1625",
        "Col9": "09-Mar-15 11:35"
    },
    {
        "id": 2,
        "Col1": "Letter",
        "Col2": "AE",
        "Col3": "H-BEAM",
        "Col4": "152X152",
        "Col5": "30.0",
        "Col6": "Beam",
        "Col7": "H-BEAM",
        "Col8": "1906",
        "Col9": "26-Nov-15 08:59"
    },
    {
        "id": 3,
        "Col1": "Letter",
        "Col2": "AE",
        "Col3": "H-BEAM",
        "Col4": "254X254",
        "Col5": "73.0",
        "Col6": "Beam",
        "Col7": "H-BEAM",
        "Col8": "1907",
        "Col9": "26-Nov-15 08:59"
    },
    {
        "id": 4,
        "Col1": "Letter",
        "Col2": "AU",
        "Col3": "CHANNEL",
        "Col4": "100 PFC",
        "Col5": "8.3",
        "Col6": "Channel",
        "Col7": "C-CHANNEL",
        "Col8": "2130",
        "Col9": "03-Sep-19 10:48"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Col1", render: function (data, type, row) { return data; } },
            { "data": "Col2", render: function (data, type, row) { return data; } },
            { "data": "Col3", render: function (data, type, row) { return data; } },
            { "data": "Col4", render: function (data, type, row) { return data; } },
            { "data": "Col5", render: function (data, type, row) { return data; } },
            { "data": "Col6", render: function (data, type, row) { return data; } },
            { "data": "Col7", render: function (data, type, row) { return data; } },
            { "data": "Col8", render: function (data, type, row) { return data; } },
            { "data": "Col9", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class NominalSizeHarmonize extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            
            <Aux>
                <Row>
                    <Col>

                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">SEARCH</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>FORM ID</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="LETTER">Letter</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>COUNTRY</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="NONE">None</option><option value="AE">AE - Unit.Arab Emir.</option><option value="Al">Al - Albania</option><option value="AR">AR - Argentina</option><option value="AS">AS - Samoa American</option><option value="AT">AT - Austria</option><option value="AU">AU - Australia</option><option value="BA">BA - Bosnia and Herzegovina</option><option value="BD">BD - Bangladesh</option><option value="BE">BE - Belgium</option><option value="BG">BG - Bulgaria</option><option value="BH">BH - Bahrain</option><option value="BN">BN - Brunei</option><option value="BR">BR - Brazil</option><option value="BY">BY - Byelorussia</option><option value="CA">CA - Canada</option><option value="CD">CD - Congo</option><option value="CI">CI - Cote d' lvoire</option><option value="CL">CL - Chile</option><option value="CN">CN - China</option><option value="Co">Co - Colombia</option><option value="CR">CR - Costa Rica</option><option value="cu">cu - Cuba</option><option value="CZ">CZ - CZECH REPUBLIC</option><option value="DE">DE - Germany</option><option value="DK">DK - DENMARK</option><option value="DM">DM - Dominican Repubilic</option><option value="DZ">DZ - Algeria</option><option value="EC">EC - Ecuador</option><option value="EE">EE - ESTONIA</option><option value="EG">EG - EGYPT</option><option value="ER">ER - Eritrea</option><option value="ES">ES - SPAIN</option><option value="ET">ET - ETHlopia</option><option value="FI">FI - FINLAND</option><option value="FJ">FJ - Fiji</option><option value="FR">FR - France</option><option value="GB">GB - Grate Britain</option><option value="GE">GE - Georgia</option><option value="GH">GH - Ghana</option><option value="GR">GR - GREECE</option><option value="GT">GT - Guatemala</option><option value="HK">HK - Hong Kong</option><option value="HN">HN - honduras</option><option value="HR">HR - Croatia</option><option value="HU">HU - HUNGARY</option><option value="ID">ID - Indonesia</option><option value="IL">IL - ISRAEL</option><option value="IN">IN - India</option><option value="IQ">IQ - IRAQ</option><option value="IR">IR - IRAN</option><option value="IT">IT - Italy</option><option value="JM">JM - Jamaica</option><option value="JO">JO - JORDAN</option><option value="JP">JP - Japan</option><option value="KE">KE - Kenya</option><option value="KH">KH - Cambodia</option><option value="KP">KP - North Korea</option><option value="KR">KR - South Korea</option><option value="KW">KW - KUWAIT</option><option value="LA">LA - Laos</option><option value="LB">LB - LEBANON</option><option value="LK">LK - Sri Lanka</option><option value="LU">LU - LUXEMBOURG</option><option value="LV">LV - LATVIA</option><option value="LY">LY - LIBYA</option><option value="MA">MA - MOROCCO</option><option value="MG">MG - Madagascar</option><option value="MK">MK - FYR Macedonia</option><option value="MM">MM - Myanmar</option><option value="MU">MU - Mauritius</option><option value="MV">MV - Maldives</option><option value="MX">MX - Mexico</option><option value="MY">MY - Malaysia</option><option value="MZ">MZ - Mozambique</option><option value="NC">NC - New Caledonia</option><option value="NG">NG - Nigeria</option><option value="NI">NI - Nicaragua</option><option value="NL">NL - NETHERLANDS</option><option value="NM">NM - Noumea</option><option value="NO">NO - Norway</option><option value="NZ">NZ - New Zealand</option><option value="OM">OM - OMAN</option><option value="PE">PE - Peru</option><option value="PG">PG - Papua Nw Guinea</option><option value="PH">PH - Philippines</option><option value="PK">PK - Pakistan</option><option value="PL">PL - POLAND</option><option value="PR">PR - Puerto Rico</option><option value="PT">PT - PORTUGAL</option><option value="PY">PY - Paraguay</option><option value="QA">QA - Qatar</option><option value="RM">RM - Sebia and Montenego</option><option value="RO">RO - ROMANIA</option><option value="RU">RU - Russia</option><option value="SA">SA - Saudi Arabia</option><option value="SB">SB - Solomon Islands</option><option value="SE">SE - SWEDEN</option><option value="SF">SF - South Africa</option><option value="SG">SG - Singapore</option><option value="SI">SI - SLOVENIA</option><option value="SK">SK - SLOVAKIA</option><option value="SL">SL - Sierra leone</option><option value="SN">SN - Senegal</option><option value="SU">SU - Samoa</option><option value="SV">SV - El salvalor</option><option value="SY">SY - SYRIA</option><option value="SZ">SZ - Switzerland</option><option value="TH">TH - Thailand</option><option value="TN">TN - TUNISIA</option><option value="TR">TR - Turkey</option><option value="TT">TT - Trinidad and tobago</option><option value="TW">TW - Taiwan</option><option value="TZ">TZ - Tanzania</option><option value="UA">UA - Ukraine</option><option value="UK">UK - United Kingdom</option><option value="US">US - United state</option><option value="UY">UY - Uruguay</option><option value="UZ">UZ - Uzbekistan</option><option value="VE">VE - Venezuela</option><option value="VN">VN - Vietnam</option><option value="VU">VU - Vanuatu</option><option value="WS">WS - Western Samoa</option><option value="YE">YE - Yemen</option><option value="ZA">ZA - South Africa</option><option value="ZW">ZW - Zimbabwe</option>
                                                            </Form.Control>    
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SECTION</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="L">ANGLE</option><option value="B">BLOOM</option><option value="C">CHANNEL</option><option value="T">CUT-BEAM</option><option value="H">H-BEAM</option><option value="P">H-PILE</option><option value="V">HVA</option><option value="I">I-BEAM</option><option value="M">MODULAR</option><option value="R">RAIL</option><option value="S">SHEET-PILE</option>
                                                            </Form.Control>    
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SHIP.YR.MTH (YYMM)</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SECTION NAME</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>NOMINAL SIZE</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>

                                            <br />
                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control as="select">
                                                                    <option value={0}>Create Date</option>
                                                                    <option value={1}>Update Date</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 1 ||
                                                            this.state.selectDate === 2 ||
                                                            this.state.selectDate === 3 ||
                                                            this.state.selectDate === 4 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 5 ||
                                                            this.state.selectDate === 6 ||
                                                            this.state.selectDate === 7 ||
                                                            this.state.selectDate === 8 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>From</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 9 ||
                                                            this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 11 ||
                                                            this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="None">None</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>At Month</option>
                                                                <option value={6}>Between Month</option>
                                                                <option value={7}>More than</option>
                                                                <option value={8}>More than or equal</option>
                                                                <option value={9}>At Quater</option>
                                                                <option value={10}>Between Quater</option>
                                                                <option value={11}>At Year</option>
                                                                <option value={12}>Between Year</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>To</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control
                                                                    as="select"
                                                                    value={this.state.supportedSelect}
                                                                    onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                                >
                                                                    <option value={0}>None</option>
                                                                    <option value={1}>At</option>
                                                                    <option value={2}>Between</option>
                                                                    <option value={3}>Less than</option>
                                                                    <option value={4}>Less than or equal</option>
                                                                    <option value={5}>More than</option>
                                                                    <option value={6}>More than or equal</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                {
                                                        this.state.selectValue === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col>
                                                    <Button className="pull-right" size="sm" onClick={e => this.handleSubmit(e)}> SEARCH </Button>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">NOMINAL SIZE HARMONIZE</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col className="btn-page text-left" sm>
                                        <Button id="btnEdit" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                        <Button id="btnDel" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                    </Col>
                                    <Col className="btn-page text-right" sm>
                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                    </Col>
                                </Row>
                                <br />
                                <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="example-select-all" /></th>
                                            <th>#</th>
                                            <th>FORM ID</th>
                                            <th>COUNTRY</th>
                                            <th>SECTION</th>
                                            <th>NOMINAL SIZE</th>
                                            <th>WEIGHT</th>
                                            <th>SECTION NAME</th>
                                            <th>NEW SECTION</th>
                                            <th>ID</th>
                                            <th>Updated Date</th>
                                        </tr>
                                    </thead>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default NominalSizeHarmonize;
