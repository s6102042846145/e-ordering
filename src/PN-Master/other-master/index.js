import React from 'react';
import {
    Row,
    Col,
    Card,
    Tab,
    Nav
} from 'react-bootstrap';
import chroma from 'chroma-js';

import Aux from "../../hoc/_Aux";

export const colourOptions = [
    { value: 'ocean', label: 'Ocean', color: '#00B8D9' },
    { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
    { value: 'purple', label: 'Purple', color: '#5243AA' },
    { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
    { value: 'orange', label: 'Orange', color: '#FF8B00' },
    { value: 'yellow', label: 'Yellow', color: '#FFC400' },
    { value: 'green', label: 'Green', color: '#36B37E', isFixed: true },
    { value: 'forest', label: 'Forest', color: '#00875A' },
    { value: 'slate', label: 'Slate', color: '#253858' },
    { value: 'silver', label: 'Silver', color: '#666666' },
];

const dot = (color = '#ccc') => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
        backgroundColor: color,
        borderRadius: 10,
        content: '" "',
        display: 'block',
        marginRight: 8,
        height: 10,
        width: 10,
    },
});

const colourStylesSingle = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? data.color : isFocused ? color.alpha(0.1).css() : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? chroma.contrast(color, 'white') > 2 ? 'white' : 'black'
                    : data.color,
            cursor: isDisabled ? 'not-allowed' : 'default',
        };
    },
    input: styles => ({ ...styles, ...dot() }),
    placeholder: styles => ({ ...styles, ...dot() }),
    singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
};

const colourStylesMulti = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? data.color : isFocused ? color.alpha(0.1).css() : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? chroma.contrast(color, 'white') > 2 ? 'white' : 'black'
                    : data.color,
            cursor: isDisabled ? 'not-allowed' : 'default',
        };
    },
    multiValue: (styles, { data }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: color.alpha(0.1).css(),
        };
    },
    multiValueLabel: (styles, { data }) => ({
        ...styles,
        color: data.color,
    }),
    multiValueRemove: (styles, { data }) => ({
        ...styles,
        color: data.color,
        ':hover': {
            backgroundColor: data.color,
            color: 'white',
        },
    }),
};

const styles = {
    multiValue: (base, state) => {
        return state.data.isFixed ? { ...base, backgroundColor: 'gray' } : base;
    },
    multiValueLabel: (base, state) => {
        return state.data.isFixed ? { ...base, fontWeight: 'bold', color: 'white', paddingRight: 6 } : base;
    },
    multiValueRemove: (base, state) => {
        return state.data.isFixed ? { ...base, display: 'none' } : base;
    }
};

const orderOptions = (values) => {
    return values.filter((v) => v.isFixed).concat(values.filter((v) => !v.isFixed));
};

class index extends React.Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            value: orderOptions([colourOptions[3], colourOptions[6], colourOptions[8]])
        };
    }

    onChange (value, { action, removedValue }) {
        switch (action) {
            case 'remove-value':
            case 'pop-value':
                if (removedValue.isFixed) {
                    return;
                }
                break;
            case 'clear':
                value = colourOptions.filter((v) => v.isFixed);
                break;
            default:
                break;

        }

        value = orderOptions(value);
        this.setState({ value: value });
    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col xl={6}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> Other Master </Card.Title>
                            </Card.Header>
                                <Nav variant="pills" className="flex-column m-0">
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/other-master/PclLicence"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>PCL Licence</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/other-master/NominalSizeHarmonize"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Nominal Size Harmonize</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/other-master/SectionHarmonize"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Section Harmonize</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/pn-master/other-master/PclLicenceDesc"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>PCL Licence Description</Nav.Link>
                                    </Nav.Item>                        
                                </Nav>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;