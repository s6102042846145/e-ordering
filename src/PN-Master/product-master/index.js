import React from 'react';
import {
    Row,
    Col,
    Card,
    Tab,
    Nav
} from 'react-bootstrap';
import chroma from 'chroma-js';

import Aux from "../../hoc/_Aux";

export const colourOptions = [
    { value: 'ocean', label: 'Ocean', color: '#00B8D9' },
    { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
    { value: 'purple', label: 'Purple', color: '#5243AA' },
    { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
    { value: 'orange', label: 'Orange', color: '#FF8B00' },
    { value: 'yellow', label: 'Yellow', color: '#FFC400' },
    { value: 'green', label: 'Green', color: '#36B37E', isFixed: true },
    { value: 'forest', label: 'Forest', color: '#00875A' },
    { value: 'slate', label: 'Slate', color: '#253858' },
    { value: 'silver', label: 'Silver', color: '#666666' },
];

const dot = (color = '#ccc') => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
        backgroundColor: color,
        borderRadius: 10,
        content: '" "',
        display: 'block',
        marginRight: 8,
        height: 10,
        width: 10,
    },
});

const colourStylesSingle = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? data.color : isFocused ? color.alpha(0.1).css() : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? chroma.contrast(color, 'white') > 2 ? 'white' : 'black'
                    : data.color,
            cursor: isDisabled ? 'not-allowed' : 'default',
        };
    },
    input: styles => ({ ...styles, ...dot() }),
    placeholder: styles => ({ ...styles, ...dot() }),
    singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
};

const colourStylesMulti = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? data.color : isFocused ? color.alpha(0.1).css() : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? chroma.contrast(color, 'white') > 2 ? 'white' : 'black'
                    : data.color,
            cursor: isDisabled ? 'not-allowed' : 'default',
        };
    },
    multiValue: (styles, { data }) => {
        const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: color.alpha(0.1).css(),
        };
    },
    multiValueLabel: (styles, { data }) => ({
        ...styles,
        color: data.color,
    }),
    multiValueRemove: (styles, { data }) => ({
        ...styles,
        color: data.color,
        ':hover': {
            backgroundColor: data.color,
            color: 'white',
        },
    }),
};

const styles = {
    multiValue: (base, state) => {
        return state.data.isFixed ? { ...base, backgroundColor: 'gray' } : base;
    },
    multiValueLabel: (base, state) => {
        return state.data.isFixed ? { ...base, fontWeight: 'bold', color: 'white', paddingRight: 6 } : base;
    },
    multiValueRemove: (base, state) => {
        return state.data.isFixed ? { ...base, display: 'none' } : base;
    }
};

const orderOptions = (values) => {
    return values.filter((v) => v.isFixed).concat(values.filter((v) => !v.isFixed));
};

class index extends React.Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
            value: orderOptions([colourOptions[3], colourOptions[6], colourOptions[8]])
        };
    }

    onChange (value, { action, removedValue }) {
        switch (action) {
            case 'remove-value':
            case 'pop-value':
                if (removedValue.isFixed) {
                    return;
                }
                break;
            case 'clear':
                value = colourOptions.filter((v) => v.isFixed);
                break;
            default:
                break;

        }

        value = orderOptions(value);
        this.setState({ value: value });
    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col xl={6}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> PRODUCT MASTER</Card.Title>
                            </Card.Header>
                                <Nav variant="pills" className="flex-column m-0">
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/SectionMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Section Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/GradeMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Grade Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/GroupVcCost"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Group VC Cost</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/GroupVcMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Group VC Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/PnStatus"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>PN Status</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/PnStatusSize"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>PN Status Size</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/SalesOrgChannelNewPn"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Sales ORG Channel New Pn</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/SectionHierarchyMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Section Hierarchy Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/SizeMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Size Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/StandardGrade"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Standard Grade</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/StandardMaster"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Standard Master</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href={"/PN-Master/Product-Master/StorageNewPn"} className='button btn btn-block text-left border-0 m-0 rounded-0 btn-outline-secondary'>Storage New PN</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;