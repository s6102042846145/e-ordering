import React from 'react';
import { Row, Col, Card, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');
require( 'datatables.net-responsive-bs' );

const names = [
    {
        "id": 1,
        "Status": "Normal",
        "SizeStd": "TIS 1227:2558 (2015)",
        "OldCodePrefix": "Z",
        "UpdateDate": "23-Feb-17 13:59",
        "SystemCode": "1"
    },
    {
        "id": 2,
        "Status": "Rusty",
        "SizeStd": "TIS 1227:2558 (2015)",
        "OldCodePrefix": "X",
        "UpdateDate": "23-Feb-17 14:01",
        "SystemCode": "2"
    },
    {
        "id": 3,
        "Status": "Scab",
        "SizeStd": "TIS 1227:2558 (2015)",
        "OldCodePrefix": "Z",
        "UpdateDate": "23-Feb-17 13:59",
        "SystemCode": "3"
    },
    {
        "id": 4,
        "Status": "Nonmove",
        "SizeStd": "TIS 1227:2558 (2015)",
        "OldCodePrefix": "Z",
        "UpdateDate": "23-Feb-17 13:59",
        "SystemCode": "4"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input style='margin-top: 0.2rem;' type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Status", render: function (data, type, row) { return data; } },
            { "data": "SizeStd", render: function (data, type, row) { return data; } },
            { "data": "OldCodePrefix", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } },
            { "data": "SystemCode", render: function (data, type, row) { return data; } }
        ],
        responsive: {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            }
        }
    });
}

class PnStatusSize extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            
            <Aux>
                <Row>
                    <Col>

                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">SEARCH</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}> PN STATUS</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="">None</option><option value="-">-PN.STATUS-</option><option value="0">Normal</option><option value="1">Rusty</option><option value="2">Scab</option><option value="3">Nonmove</option><option value="4">Cut</option><option value="5">CRB</option><option value="6">Bolt&amp;Nut</option><option value="7">CRB&amp;CUT</option><option value="8">Bolt&amp;Nut SLOT</option><option value="98">-PN.STATUS-</option><option value="99">-PN.STATUS-</option><option value="A">Off Shore</option><option value="B">Off Shore II</option><option value="C">Off Shore Rusty</option><option value="D">Off Shore II Rusty</option><option value="E">Rusty&amp;cut</option><option value="G">Galvanise</option><option value="H">Cut SP/2</option><option value="I">Cut SP/3</option><option value="J">Cut SP/4</option><option value="K">Drill</option><option value="L">Coat</option><option value="M">Drill&amp;Coat</option><option value="N">Drill&amp;Coat non toxic</option><option value="O">Cut SP/4+Coat</option><option value="P">Cut SP/2+Drill</option><option value="Q">Cut SP/3+Drill</option><option value="R">Cut SP/4+Drill</option><option value="S">Coat non toxic</option><option value="T">Drill-Galvanise</option><option value="U">Curved</option><option value="V">Primer Budget</option><option value="W">Primer Standard</option><option value="X">Primer Premium</option><option value="Y">Grooved</option><option value="Z">Actual weight</option><option value="ก">Cut-Galvanise</option><option value="ข">Bevel</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SIZE.STD</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="ALL">All</option><option value="">None</option><option value="ABS">ASTM A6/A6M:2003</option><option value="ASTM2003">ASTM A6/A6M:2003</option><option value="BS">BS EN 10034:1993</option><option value="BS1">BSEN 10056-1:1999</option><option value="BS3">BS EN 10279:2000</option><option value="EURO">EN 53-1962,EN 19-1957</option><option value="GB706">GB/T 706-2008</option><option value="GOST">GOST 380-94</option><option value="JIS1990">JIS G3192-1990</option><option value="JIS1994">JIS G3192-1994</option><option value="JISA5528">JIS A5528-2006</option><option value="JISA5528-2012">JIS A5528 : 2012</option><option value="SNI-C">SNI-C : SNI 07-0052-2006</option><option value="SNI-H">SNI-H : SNI 2610-2011</option><option value="SNI-I">SNI-I : SNI 07-0329-2005</option><option value="SNI-L">SNI-L : SNI 07-2054-2006</option><option value="SNI-WF">SNI-WF : SNI 07-7178-2006</option><option value="TIS">TIS 1227:1996</option><option value="TIS/JIS">TIS 1227:1996/JIS G3192:1990</option><option value="TIS1390">TIS1390-2560 (2017)</option><option value="TIS1390-2539">TIS1390-2539 (1996)</option><option value="TIS2015">TIS 1227:2558 (2015)</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>OLD CODE PREFIX</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SYSTEM CODE</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>

                                            <br />
                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control as="select">
                                                                    <option value={0}>Create Date</option>
                                                                    <option value={1}>Update Date</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 1 ||
                                                            this.state.selectDate === 2 ||
                                                            this.state.selectDate === 3 ||
                                                            this.state.selectDate === 4 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 5 ||
                                                            this.state.selectDate === 6 ||
                                                            this.state.selectDate === 7 ||
                                                            this.state.selectDate === 8 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>From</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 9 ||
                                                            this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 11 ||
                                                            this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="None">None</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>At Month</option>
                                                                <option value={6}>Between Month</option>
                                                                <option value={7}>More than</option>
                                                                <option value={8}>More than or equal</option>
                                                                <option value={9}>At Quater</option>
                                                                <option value={10}>Between Quater</option>
                                                                <option value={11}>At Year</option>
                                                                <option value={12}>Between Year</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>To</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control
                                                                    as="select"
                                                                    value={this.state.supportedSelect}
                                                                    onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                                >
                                                                    <option value={0}>None</option>
                                                                    <option value={1}>At</option>
                                                                    <option value={2}>Between</option>
                                                                    <option value={3}>Less than</option>
                                                                    <option value={4}>Less than or equal</option>
                                                                    <option value={5}>More than</option>
                                                                    <option value={6}>More than or equal</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                {
                                                        this.state.selectValue === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col>
                                                    <Button className="pull-right" size="sm" onClick={e => this.handleSubmit(e)}> SEARCH </Button>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">PN STATUS - SIZE.STD - OLD CODE</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col className="btn-page text-left" sm>
                                        <Button id="btnEdit" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                        <Button id="btnDel" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                    </Col>
                                    <Col className="btn-page text-right" sm>
                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                    </Col>
                                </Row>
                                <br />
                                <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="example-select-all" /></th>
                                            <th>#</th>
                                            <th>STATUS</th>
                                            <th>SIZE.STD</th>
                                            <th>OLD CODE PREFIX</th>
                                            <th>UPDATED DATE</th>
                                            <th>SYSTEM CODE</th>
                                        </tr>
                                    </thead>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default PnStatusSize;
