import React from 'react';
import { Row, Col, Card, Form, Button, Table, Modal, InputGroup, FormControl } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Datetime from 'react-datetime';
import NumberFormat from 'react-number-format';
import withReactContent from 'sweetalert2-react-content';

import Aux from "../../hoc/_Aux";

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;

$.DataTable = require('datatables.net-bs');


const names = [
    {
        "id": 1,
        "Std": "ABS",
        "StdDesc": "ABS Materials and Welding",
        "SizeStdDessc": "ASTM A6/A6M:2003",
        "Size": "Yes",
        "Grade": "Yes",
        "SysCode": "40",
        "UpdateDate": "27-Aug-15 15:53"
    },
    {
        "id": 2,
        "Std": "AS/NZS",
        "StdDesc": "AS/NZS 3679.1:2016",
        "SizeStdDessc": "AS/NZS 3679.1-2016",
        "Size": "No",
        "Grade": "Yes",
        "SysCode": "1",
        "UpdateDate": "07-Feb-17 17:45"
    },
    {
        "id": 3,
        "Std": "ASTM",
        "StdDesc": "ASTM A36/A36M:2005",
        "SizeStdDessc": "ASTM A6/A6M:2003",
        "Size": "No",
        "Grade": "Yes",
        "SysCode": "2",
        "UpdateDate": "16-Mar-15 11:04"
    },
    {
        "id": 4,
        "Std": "ASTM2",
        "StdDesc": "ASTM A992/A992M:2004a",
        "SizeStdDessc": "ASTM A6/A6M:2003",
        "Size": "Yes",
        "Grade": "Yes",
        "SysCode": "3",
        "UpdateDate": "16-Mar-15 11:00"
    }
];

function atable() {
    let tableZero = '#data-table-zero';
    $.fn.dataTable.ext.errMode = 'throw';

    $(tableZero).DataTable({
        data: names,
        lengthChange: false,
        searching: false,
        order: [[1, "asc"]],
        columns: [
            {
                sortable: false,
                className: "text-center",
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='id[]' value='" + $('<div/>').text(data).html() + "'>"
                }
            },
            { "data": "id", render: function (data, type, row) { return data; } },
            { "data": "Std", render: function (data, type, row) { return data; } },
            { "data": "StdDesc", render: function (data, type, row) { return data; } },
            { "data": "SizeStdDessc", render: function (data, type, row) { return data; } },
            { "data": "Size", render: function (data, type, row) { return data; } },
            { "data": "Grade", render: function (data, type, row) { return data; } },
            { "data": "SysCode", render: function (data, type, row) { return data; } },
            { "data": "UpdateDate", render: function (data, type, row) { return data; } }
        ]
    });
}

class StandardMaster extends React.Component {
    state = {
        supportedCheckbox: false,
        supportedRadio: false,
        isModal: false,
        setTitleModal: "",
        selectDate: 0,
        selectValue: 0
    };

    renderInput = (props, openCalendar, closeCalendar) => {
        return (
            <div>
                <InputGroup>
                    <FormControl type="text" {...props} />
                    <InputGroup.Append>
                        <InputGroup.Text onClick={openCalendar} style={{ cursor: 'pointer' }}><i className="fa fa-calendar text-primary" /></InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>

            </div>
        );
    };

    supportedSelectHandler = (event, type) => {
        if (type === "Date") {
            this.setState({ selectDate: parseInt(event.target.value) });
        } else {
            this.setState({ selectValue: parseInt(event.target.value) });
        }
    };

    setShowModal = (event, type) => {
        if (type === "Create") {
            this.setState({ setTitleModal: "เพิ่มข้อมูล" })
        } else {
            this.setState({ setTitleModal: "แก้ไขข้อมูล" })
        }

        this.setState({ isModal: true });
    };

    sweetConfirmHandler = () => {
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'คุณต้องการลบข้อมูลนี้ หรือไม่ ?',
            text: 'เมื่อลบแล้ว คุณจะไม่สามารถกู้คือข้อมูลนี้ได้',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true
        }).then((willDelete) => {
            if (willDelete.value) {
                return MySwal.fire('', 'ลบข้อมูลสำเร็จแล้ว !', 'success');
            } else {
                return MySwal.fire('', 'ยกเลิกการลบข้อมูลนี้ !', 'error');
            }
        });
    };
    
    componentDidMount() {
        atable()
    };

    render() {

        $('#example-select-all').click(function (event) {

            if (this.checked) {
                $(':checkbox').each(function () {
                    $('#btnDel').removeClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    $('#btnDel').addClass('d-none');
                    $('#btnEdit').addClass('d-none');
                    this.checked = false;
                });
            }
        });

        $('#data-table-zero tbody').on('click', 'input[type="checkbox"]', function (event) {

            var $checkboxes = $('#data-table-zero tbody input[type="checkbox"]');
            var numberOfChecked = $checkboxes.filter(':checked').length
            var totalCheckboxes = $checkboxes.length;

            var numberNotChecked = totalCheckboxes - numberOfChecked;

            if (numberOfChecked === totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', true);
            } else if (numberOfChecked === 1) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').removeClass('d-none');
                $('#example-select-all').prop('checked', false);
            }
            else if (numberNotChecked < totalCheckboxes) {
                $('#btnDel').removeClass('d-none');
                $('#btnEdit').addClass('d-none');
                $('#example-select-all').prop('checked', false);
            } else {
                $('#btnDel').addClass('d-none');
                $('#btnEdit').addClass('d-none');
            }
        });

        return (
            
            <Aux>
                <Row>
                    <Col>

                        <Modal size="lg" show={this.state.isModal} onHide={() => this.setState({ isModal: false })}>
                            <Modal.Header closeButton>
                                <Modal.Title as="h5">{this.state.setTitleModal}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>...</Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.setState({ isModal: false })}>Close</Button>
                                <Button variant="primary">Save Changes</Button>
                            </Modal.Footer>
                        </Modal>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">SEARCH</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={12}>
                                        <Form>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>STD</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>STD_DESC</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>SIZE STD_DESC</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control type="text" />
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>
                                            <Form.Group as={Row}>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}></Form.Label>
                                                        <Col sm={8}></Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Grade Flag</Form.Label>
                                                        <Col sm={8}>
                                                        <Form.Control as="select">
                                                        <option value="01">All</option>
                                                                <option value="02">No</option>
                                                                <option value="03">Yes</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={4}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Size Flag</Form.Label>
                                                        <Col sm={8}>
                                                        <Form.Control as="select">
                                                                <option value="01">All</option>
                                                                <option value="02">No</option>
                                                                <option value="03">Yes</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                            </Form.Group>

                                            <br />
                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control as="select">
                                                                    <option value={0}>Create Date</option>
                                                                    <option value={1}>Update Date</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 1 ||
                                                            this.state.selectDate === 2 ||
                                                            this.state.selectDate === 3 ||
                                                            this.state.selectDate === 4 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 5 ||
                                                            this.state.selectDate === 6 ||
                                                            this.state.selectDate === 7 ||
                                                            this.state.selectDate === 8 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>From</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 9 ||
                                                            this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 11 ||
                                                            this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Search by numeric</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control as="select">
                                                                <option value="None">None</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectValue === 1 ||
                                                        this.state.selectValue === 2 ||
                                                        this.state.selectValue === 3 ||
                                                        this.state.selectValue === 4 ||
                                                        this.state.selectValue === 5 ||
                                                        this.state.selectValue === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>From Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />  
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Control
                                                                as="select"
                                                                value={this.state.supportedSelect}
                                                                onChange={(event) => this.supportedSelectHandler(event, "Date")}
                                                            >
                                                                <option value={0}>None</option>
                                                                <option value={1}>At</option>
                                                                <option value={2}>Between</option>
                                                                <option value={3}>Less than</option>
                                                                <option value={4}>Less than or equal</option>
                                                                <option value={5}>At Month</option>
                                                                <option value={6}>Between Month</option>
                                                                <option value={7}>More than</option>
                                                                <option value={8}>More than or equal</option>
                                                                <option value={9}>At Quater</option>
                                                                <option value={10}>Between Quater</option>
                                                                <option value={11}>At Year</option>
                                                                <option value={12}>Between Year</option>
                                                            </Form.Control>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                    {
                                                        this.state.selectDate === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Datetime renderInput={this.renderInput} timeFormat={false} inputProps={{ placeholder: 'Select Date' }} />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 6 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={3}>To</Form.Label>
                                                                <Col sm={4}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                                <Col sm={5}>
                                                                    <Form.Control as="select">
                                                                        <option value="ALL">All</option><option value="01">01 - January</option><option value="02">02 - Febuary</option><option value="03">03 - March</option><option value="04">04 - April</option><option value="05">05 - May</option><option selected value="06">06 - June</option><option value="07">07 - July</option><option value="08">08 - August</option><option value="09">09 - September</option><option value="10">10 - October</option><option value="11">11 - November</option><option value="12">12 - December</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 10 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="01">Quater 1</option><option value="02">Quater 2</option><option value="03">Quater 3</option><option value="04">Quater 4</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                    {
                                                        this.state.selectDate === 12 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To</Form.Label>
                                                                <Col sm={8}>
                                                                    <Form.Control as="select">
                                                                        <option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option selected value="2021">2021</option><option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option>
                                                                    </Form.Control>
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                                <Col sm={3}>
                                                    <Form.Group as={Row}>
                                                        <Form.Label column sm={4}>Criteria</Form.Label>
                                                        <Col sm={8}>
                                                            <Form.Group>
                                                                <Form.Control
                                                                    as="select"
                                                                    value={this.state.supportedSelect}
                                                                    onChange={(event) => this.supportedSelectHandler(event, "Value")}
                                                                >
                                                                    <option value={0}>None</option>
                                                                    <option value={1}>At</option>
                                                                    <option value={2}>Between</option>
                                                                    <option value={3}>Less than</option>
                                                                    <option value={4}>Less than or equal</option>
                                                                    <option value={5}>More than</option>
                                                                    <option value={6}>More than or equal</option>
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Form.Group>
                                                </Col>
                                                <Col sm={3}>
                                                {
                                                        this.state.selectValue === 2 ?

                                                            <Form.Group as={Row}>
                                                                <Form.Label column sm={4}>To Value</Form.Label>
                                                                <Col sm={8}>
                                                                    <NumberFormat className="form-control" thousandSeparator={true} placeholder="0" />
                                                                </Col>
                                                            </Form.Group>

                                                            : ''
                                                    }
                                                </Col>
                                            </Form.Group>

                                            <Form.Group as={Row}>
                                                <Col>
                                                    <Button className="pull-right" size="sm" onClick={e => this.handleSubmit(e)}> SEARCH </Button>
                                                </Col>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">STANDARD MASTER</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col className="btn-page text-left" sm>
                                        <Button id="btnEdit" variant="warring" className="btn btn-icon btn-rounded btn-outline-warning d-none" onClick={e => this.setShowModal(e, "Edit")}><span className="fa fa-edit"/></Button>
                                        <Button id="btnDel" variant="default" className="btn btn-icon btn-rounded btn-outline-danger d-none" onClick={this.sweetConfirmHandler}><span className="fa fa-trash"/></Button>
                                    </Col>
                                    <Col className="btn-page text-right" sm>
                                        <Button size="sm" variant="danger" >SET SIZE</Button>
                                        <Button size="sm" variant="success" >SET UN-SIZE</Button>
                                        <Button size="sm" variant="danger" >SET GRADE</Button>
                                        <Button size="sm" variant="success" >SET UN-GRADE</Button>
                                        <Button size="sm" variant="primary" onClick={e => this.setShowModal(e, "Create")}>เพิ่มข้อมมูล</Button>
                                    </Col>
                                </Row>
                                <br />
                                <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                    <thead>
                                        <tr>
                                            <th><Form.Check id="example-select-all" /></th>
                                            <th>#</th>
                                            <th>STD</th>
                                            <th>STD_DESC</th>
                                            <th>SIZE STD_DESC</th>
                                            <th>SIZE</th>
                                            <th>GRADE</th>
                                            <th>SYS.CODE</th>
                                            <th>UPDATED DATE</th>
                                        </tr>
                                    </thead>
                                </Table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default StandardMaster;
