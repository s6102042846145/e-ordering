import React from 'react';
import {
    Row,
    Col,
    Card,
    Nav
} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class index extends React.Component {

    

    render() {

        return (
            <Aux>
                <Row>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5"> PRODUCT </Card.Title>
                            </Card.Header>
                            <Row className="mb-5">
                                <Col sm={6} className="col-sm-6 mb-n4">
                                    <div className="card m-15" >
                                    <a className="list-group-item list-group-item-action" href={"/PN-Master/Product/ProductCodeOpenningMS126"}>
                                        ใบเปิดรหัสสินค้า (MS126-1) 
                                        <i className="feather float-right icon-chevron-right mt-1"></i>
                                    </a>
                                    <a className="list-group-item list-group-item-action" href={"/PN-Master/Product/ConfirmOpenProductSBC"}>
                                        ยืนยันเปิดรหัสสินค้า (สบช)
                                        <i className="feather float-right icon-chevron-right mt-1"></i>
                                    </a>
                                    <a className="list-group-item list-group-item-action" href={"/PN-Master/Product/ConfirmOpenProductSSK"}>
                                        ยืนยันเปิดรหัสสินค้า (สสค)
                                        <i className="feather float-right icon-chevron-right mt-1"></i>
                                    </a>
                                    <a className="list-group-item list-group-item-action" href={"/PN-Master/Product/ConfirmOpenProductSSR2"}>
                                        ยืนยันเปิดรหัสสินค้า (สสร2)
                                        <i className="feather float-right icon-chevron-right mt-1"></i>
                                    </a>
                                    </div>
                                </Col>
                                <Col sm={6}>
                                <div className="card m-15" >
                                    <a className="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href={"/PN-Master/Product/ConfirmOpenProductSGT"} role="tab" aria-controls="home">ยืนยันเปิดรหัสสินค้า (สกธ) <span className="badge badge-pill badge-primary float-sm-right">14</span></a>
                                    <a className="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href={"/PN-Master/Product/ConfirmOpenProductSSJ"} role="tab" aria-controls="profile">ยืนยันเปิดรหัสสินค้า (สสจ)</a>
                                    <a className="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href={"/PN-Master/Product/ConfirmOpenProductSSR1"} role="tab" aria-controls="messages">ยืนยันเปิดรหัสสินค้า (สสร1)</a>
                                    <a className="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href={"/PN-Master/Product/ItemMastFGSTOCK"} >ITEM MAST (FGSTOCK)</a>
                                    </div>
                                </Col>
                            </Row>
                                
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default index;